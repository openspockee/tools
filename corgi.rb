# typed: false
# frozen_string_literal: true

# This file was generated by GoReleaser. DO NOT EDIT.
class Corgi < Formula
  desc "Command line interface for database configurations and other useful things, written in Go"
  homepage "https://gitlab.com/openspockee/corgi"
  version "2.0.2"

  on_macos do
    if Hardware::CPU.arm?
      url "https://gitlab.com/openspockee/corgi/-/releases/v2.0.2/downloads/corgi_2.0.2_Darwin_arm64.tar.gz"
      sha256 "bd421b5810abb1ef49bf366e79f914bc3ece5c330297602a790ac1ad47fb2042"

      def install
        bin.install "corgi"
      end
    end
    if Hardware::CPU.intel?
      url "https://gitlab.com/openspockee/corgi/-/releases/v2.0.2/downloads/corgi_2.0.2_Darwin_x86_64.tar.gz"
      sha256 "a4123516e2d26af16a28aa4601a375193511bb5908a113f2dcd61a6ba7b982fc"

      def install
        bin.install "corgi"
      end
    end
  end

  on_linux do
    if Hardware::CPU.arm? && Hardware::CPU.is_64_bit?
      url "https://gitlab.com/openspockee/corgi/-/releases/v2.0.2/downloads/corgi_2.0.2_Linux_arm64.tar.gz"
      sha256 "684d398b1a2bd9d84966d4c828730fc3cbbea857a2591d45bda2f860a5bacea4"

      def install
        bin.install "corgi"
      end
    end
    if Hardware::CPU.intel?
      url "https://gitlab.com/openspockee/corgi/-/releases/v2.0.2/downloads/corgi_2.0.2_Linux_x86_64.tar.gz"
      sha256 "a7461f452751b81e8831eb5fcb6ba100a63e037e37ff213c5643d5848ab242ca"

      def install
        bin.install "corgi"
      end
    end
  end
end
